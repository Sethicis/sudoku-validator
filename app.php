#!/usr/bin/env php
<?php

require_once __DIR__.'/vendor/autoload.php';

use Sudoku\Validator\Model\SudokuGrid;
use Sudoku\Validator\Support\FileLoader;

if ($argc < 2) {
    throw new ArgumentCountError('You must provide a filename as the first program argument when executing.');
}

$loader = new FileLoader();
$set = $loader->load($argv[1]);

$sudokuValidator = new SudokuGrid($set);

if ($sudokuValidator->validate() === true) {
    print_r("Success! You provided a valid Sudoku solution.  Good for you. :)\n");
} else {
    print_r("Failure! It looks like there may be an issue with your Sudoku solution. Please double check it and try again. :(\n");
}
