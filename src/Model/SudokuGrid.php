<?php

namespace Sudoku\Validator\Model;

/**
 * Class SudokuGrid.
 */
final class SudokuGrid
{
    /**
     * @var - Valid Sudoku set used in validation.
     */
    private const VALID_SET = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    private $validatedZones = [];

    /**
     * @var array
     */
    private $internalGrid = [];

    /**
     * SudokuGrid constructor.
     *
     * @param array $internalGrid
     */
    public function __construct(array $internalGrid)
    {
        $this->internalGrid = $internalGrid;
        $this->validatedZones = array_fill_keys(range(1, 9), false);
    }

    public function validate(): bool
    {
        foreach ($this->internalGrid as $rowIdx => $row) {
            $zoneSet = $this->getSetForZone($rowIdx+1);
            $column = array_column($this->internalGrid, $rowIdx);
            if (!$this->isValidSet($row) || !$this->isValidSet($column) || !$this->isValidSet($zoneSet)) {
                return false;
            }
        }

        return true;
    }

    private function isValidSet(array $values): bool
    {
        sort($values);

        return self::VALID_SET == $values;
    }

    private function getSetForZone(int $zone): array
    {
        switch($zone) {
            case 1:
                return [
                    $this->internalGrid[0][0], $this->internalGrid[0][1], $this->internalGrid[0][2],
                    $this->internalGrid[1][0], $this->internalGrid[1][1], $this->internalGrid[1][2],
                    $this->internalGrid[2][0], $this->internalGrid[2][1], $this->internalGrid[2][2],
                ];
            case 2:
                return [
                    $this->internalGrid[0][3], $this->internalGrid[0][4], $this->internalGrid[0][5],
                    $this->internalGrid[1][3], $this->internalGrid[1][4], $this->internalGrid[1][5],
                    $this->internalGrid[2][3], $this->internalGrid[2][4], $this->internalGrid[2][5],
                ];
            case 3:
                return [
                    $this->internalGrid[0][6], $this->internalGrid[0][7], $this->internalGrid[0][8],
                    $this->internalGrid[1][6], $this->internalGrid[1][7], $this->internalGrid[1][8],
                    $this->internalGrid[2][6], $this->internalGrid[2][7], $this->internalGrid[2][8],
                ];
            case 4:
                return [
                    $this->internalGrid[3][0], $this->internalGrid[3][1], $this->internalGrid[3][2],
                    $this->internalGrid[4][0], $this->internalGrid[4][1], $this->internalGrid[4][2],
                    $this->internalGrid[5][0], $this->internalGrid[5][1], $this->internalGrid[5][2],
                ];
            case 5:
                return [
                    $this->internalGrid[3][3], $this->internalGrid[3][4], $this->internalGrid[3][5],
                    $this->internalGrid[4][3], $this->internalGrid[4][4], $this->internalGrid[4][5],
                    $this->internalGrid[5][3], $this->internalGrid[5][4], $this->internalGrid[5][5],
                ];
            case 6:
                return [
                    $this->internalGrid[3][6], $this->internalGrid[3][7], $this->internalGrid[3][8],
                    $this->internalGrid[4][6], $this->internalGrid[4][7], $this->internalGrid[4][8],
                    $this->internalGrid[5][6], $this->internalGrid[5][7], $this->internalGrid[5][8],
                ];
            case 7:
                return [
                    $this->internalGrid[6][0], $this->internalGrid[6][1], $this->internalGrid[6][2],
                    $this->internalGrid[7][0], $this->internalGrid[7][1], $this->internalGrid[7][2],
                    $this->internalGrid[8][0], $this->internalGrid[8][1], $this->internalGrid[8][2],
                ];
            case 8:
                return [
                    $this->internalGrid[6][3], $this->internalGrid[6][4], $this->internalGrid[6][5],
                    $this->internalGrid[7][3], $this->internalGrid[7][4], $this->internalGrid[7][5],
                    $this->internalGrid[8][3], $this->internalGrid[8][4], $this->internalGrid[8][5],
                ];
            case 9:
                return [
                    $this->internalGrid[6][6], $this->internalGrid[6][7], $this->internalGrid[6][8],
                    $this->internalGrid[7][6], $this->internalGrid[7][7], $this->internalGrid[7][8],
                    $this->internalGrid[8][6], $this->internalGrid[8][7], $this->internalGrid[8][8],
                ];
            default:
                // This shouldn't happen.  We could raise an exception here, but for simplicity we won't bother.
                return [];
        }
    }

    /**
     * Returns a sudoku zone from 1 to 9.
     *
     * @param int $row
     * @param int $column
     *
     * @return int
     */
    private function getZoneForPos(int $row, int $column): int
    {
        // TODO: Remove not needed.
        $tuple = [$row, $column];
        switch($tuple) {
            case $tuple[0] < 3 && $tuple[1] < 3:
                return 1;
            case $tuple[0] < 3 && $tuple[1] < 6:
                return 2;
            case $tuple[0] < 3:
                return 3;
            case $tuple[0] < 6 && $tuple[1] < 3:
                return 4;
            case $tuple[0] < 6 && $tuple[1] < 6:
                return 5;
            case $tuple[0] < 6:
                return 6;
            case $tuple[0] > 5 && $tuple[1] < 3:
                return 7;
            case $tuple[0] > 5 && $tuple[1] < 6:
                return 8;
            default:
                // Obviously, if we receive a non-standard sudoku structure this will fail, but we aren't validating structure.
                return 9;
        }
    }

    private function zoneHasBeenValidated(int $zone): bool
    {
        // TODO: Remove not needed.
        return $this->validatedZones[$zone];
    }
}
