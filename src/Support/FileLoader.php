<?php

namespace Sudoku\Validator\Support;

use Sudoku\Validator\Exceptions\MissingOrInvalidFile;

/**
 * Class FileLoader.
 */
final class FileLoader
{
    /**
     * @param string $filename
     *
     * @return array
     * @throws MissingOrInvalidFile
     */
    public function load(string $filename): array
    {
        if (!file_exists($filename)) {
            throw new MissingOrInvalidFile($filename);
        }

        $contents = file_get_contents($filename);
        $contentsAsLines = explode("\n", $contents);
        foreach ($contentsAsLines as &$line) {
            if (!$line) {
                continue;
            }
            $line = array_map('intval', explode(' ', $line));
        }
        $contentsAsLines = array_filter($contentsAsLines);

        return $contentsAsLines;
    }
}
