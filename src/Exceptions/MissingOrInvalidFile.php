<?php

namespace Sudoku\Validator\Exceptions;

use Exception;

/**
 * Class MissingOrInvalidFile.
 */
final class MissingOrInvalidFile extends Exception
{
    /**
     * MissingOrInvalidFile constructor.
     *
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        parent::__construct(sprintf('Failed to load file "%s".  Please validate that the file path provided is correct and that the file is readable.', $filename));
    }
}
