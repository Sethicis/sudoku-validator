<?php

namespace Sudoku\Validator\Tests\Support;

use Sudoku\Validator\Exceptions\MissingOrInvalidFile;
use Sudoku\Validator\Support\FileLoader;
use PHPUnit\Framework\TestCase;

/**
 * Class FileLoaderTest.
 *
 * Unit Test.
 */
final class FileLoaderTest extends TestCase
{
    /**
     * @var - Test file used in validating loading format.
     */
    private const TEST_FILE_TO_LOAD = __DIR__.'/../../../test_load_file.txt';

    /**
     * @var - Invalid file used to test exception use case.
     */
    private const INVALID_FILE = 'foobar.txt';

    /**
     * @param string      $filename
     * @param string|null $expectedException
     *
     * @throws MissingOrInvalidFile
     *
     * @dataProvider loadDataProvider
     */
    public function testLoad(string $filename, string $expectedException = null)
    {
        if ($expectedException) {
            $this->expectException($expectedException);
        }
        $loader = new FileLoader();
        $result = $loader->load($filename);
        $values = range(1, 81);
        $expected = array_chunk($values, 9);

        self::assertSame($expected, $result);
    }

    /**
     * @return array
     */
    public function loadDataProvider(): array
    {
        return [
            [self::TEST_FILE_TO_LOAD],
            [self::INVALID_FILE, MissingOrInvalidFile::class],
        ];
    }
}
