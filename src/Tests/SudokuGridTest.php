<?php

namespace Sudoku\Validator\Tests;

use Sudoku\Validator\Exceptions\MissingOrInvalidFile;
use Sudoku\Validator\Model\SudokuGrid;
use PHPUnit\Framework\TestCase;
use Sudoku\Validator\Support\FileLoader;

/**
 * Class SudokuGridTest.
 *
 * Unit Test.
 */
final class SudokuGridTest extends TestCase
{
    /**
     * @param array $sudokuSet
     * @param bool  $isValid
     *
     * @dataProvider validateDataProvider
     */
    public function testValidate(array $sudokuSet, bool $isValid)
    {
        $sudokuGrid = new SudokuGrid($sudokuSet);

        self::assertSame($isValid, $sudokuGrid->validate());
    }

    /**
     * @return array
     *
     * @throws MissingOrInvalidFile
     */
    public function validateDataProvider(): array
    {
        $fileLoader = new FileLoader();

        $goodSet = $fileLoader->load(__DIR__.'/../../sudoku.txt');
        $badSet = $fileLoader->load(__DIR__.'/../../sudoku_bad1.txt');

        return [
            [$goodSet, true],
            [$badSet, false],
        ];
    }
}
