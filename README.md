# Sudoku Solution Validator

## Setup

To make sure that you have all necessary dependencies be sure to run the following command before usage.
> composer install

## How to Use

The applications entry point is via the [app.php](app.php) file.

app.php expects one argument, which should be the file containing the Sudoku solution you want it to validate.
The Sudoku solution to validate is expected to be represented in a plain text file with each row of numbers delimited by a newline character.

An example input Sudoku solution can be found in the [sudoku.txt](sudoku.txt) file.

### Example Usage

app.php sudoku.txt

## Expected Output

If the Sudoku solution provided was CORRECT you will see the following output:
>Success! You provided a valid Sudoku solution.  Good for you. :)

If the Sudoku solution provided was INCORRECT you will see the following output:
>Failure! It looks like there may be an issue with your Sudoku solution. Please double check it and try again. :(
